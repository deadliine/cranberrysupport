package ca.cranberry.app.view;

import static ca.cranberry.app.utils.IColor.COMM_BACKGROUND;
import static ca.cranberry.app.utils.ISize.COMM_COLUMN;
import static ca.cranberry.app.utils.ISize.COMM_ROW;
import static ca.cranberry.app.utils.ISize.HORIZONTAL_COMMS_GAP_CONNEXION;
import static ca.cranberry.app.utils.ISize.HORIZONTAL_COMMS_SIZE_CONNEXION;
import static ca.cranberry.app.utils.ISize.HORIZONTAL_REQUETES_GAP_CONNEXION;
import static ca.cranberry.app.utils.ISize.HORIZONTAL_REQUETES_SIZE_CONNEXION;
import static ca.cranberry.app.utils.ISize.HORIZONTAL_REQUETE_SIZE_CONNEXION;
import static ca.cranberry.app.utils.ISize.VERTICAL_AJOUT_REQUETE_GAP_CONNEXION;
import static ca.cranberry.app.utils.ISize.VERTICAL_COMM_SIZE_CONNEXION;
import static ca.cranberry.app.utils.ISize.VERTICAL_MODIFIER_GAP_CONNEXION;
import static ca.cranberry.app.utils.ISize.VERTICAL_REQUETES_SIZE_CONNEXION;
import static ca.cranberry.app.utils.ISize.VERTICAL_REQUETE_GAP_CONNEXION;
import static ca.cranberry.app.utils.ISize.VERTICAL_REQUETE_SIZE_CONNEXION;
import static ca.cranberry.app.utils.IView.DEFAULT_STRING;
import static ca.cranberry.app.utils.IView.LABEL_ADD_COMM;
import static ca.cranberry.app.utils.IView.LABEL_ADD_FILE;
import static ca.cranberry.app.utils.IView.LABEL_ADD_REQUETE;
import static ca.cranberry.app.utils.IView.LABEL_CATEGORIE;
import static ca.cranberry.app.utils.IView.LABEL_DESCRIPTION;
import static ca.cranberry.app.utils.IView.LABEL_INDEX_REQUETES;
import static ca.cranberry.app.utils.IView.LABEL_QUIT;
import static ca.cranberry.app.utils.IView.LABEL_REQUETES_EMPTY;
import static ca.cranberry.app.utils.IView.LABEL_SEE_COMMS;
import static ca.cranberry.app.utils.IView.LABEL_SEE_REQUETE;
import static ca.cranberry.app.utils.IView.LABEL_STATUT;
import static ca.cranberry.app.utils.IView.LABEL_SUJET;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.AbstractListModel;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle;
import javax.swing.WindowConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import ca.cranberry.app.beans.Commentaire;
import ca.cranberry.app.beans.Requete;
import ca.cranberry.app.beans.Utilisateur;
import ca.cranberry.app.service.BanqueRequetes;

public class FenetreClientConnection extends javax.swing.JFrame {

    private static final long serialVersionUID = 8802609438589888833L;
    private Utilisateur utilisateur;
    private List<Requete> requetes;
    private Requete requeteToShow;
    private JFrame frame;
    private File file;
    private boolean showComments = false;
    private JButton ajouteRequeteButton;
    private JTextArea commentaireZone;
    private JButton fichierButton;
    private JFileChooser fileChooser;
    private JLabel modifierJlabel;
    private JScrollPane requetesJScroll;
    private JScrollPane requeteJScroll;
    private JScrollPane commentaireJScroll;
    private JList<String> requetesJList;
    private JButton modificationButton;
    private JButton quitterButton;
    private JTextArea requeteZone;
    private JLabel requetesJLabel;
    private JButton commentaireButton;

    public FenetreClientConnection(Utilisateur client) {
        initializeRequete();
        initializeQuitterButton();
        initializeModificationButton();
        initializeFichierButton();
        initializeCommentaire();
        initComponents();
        this.setVisible(true);
        utilisateur = client;
        requetes = client.getRequetes();

        DefaultListModel<String> defaultListModel = new DefaultListModel<String>();

        if (client.getRequetes().isEmpty()) {
            defaultListModel.addElement(LABEL_REQUETES_EMPTY);
        } else {
            for (int i = 0; i < client.getRequetes().size(); i++) {
                defaultListModel.addElement(client.getRequetes().get(i).getSujet());

            }

        }
        requetesJList.setModel(defaultListModel);
        commentaireZone.setVisible(false);
    }

    private void initComponents() {

        fileChooser = new JFileChooser();
        modifierJlabel = new JLabel();

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        modifierJlabel.setText("Modifier la requête sélectionnée:");

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layoutSetHorizontal(layout);
        layoutSetVertical(layout);

        pack();
    }

    private void initializeFichierButton() {
        fichierButton = new JButton();
        fichierButton.setText(LABEL_ADD_FILE);
        fichierButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                fichierButtonActionPerformed(evt);
            }
        });
    }

    private void initializeModificationButton() {
        modificationButton = new JButton();
        modificationButton.setText(LABEL_ADD_COMM);
        modificationButton.setEnabled(false);
        modificationButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                modificationButtonActionPerformed(evt);
            }
        });
    }

    private void initializeQuitterButton() {
        quitterButton = new JButton();
        quitterButton.setText(LABEL_QUIT);
        quitterButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                quitterButtonActionPerformed(evt);
            }
        });
    }

    private void initializeRequete() {
        requetesJScroll = new JScrollPane();
        requetesJList = new JList<String>();
        requetesJLabel = new JLabel();
        ajouteRequeteButton = new JButton();
        requeteJScroll = new JScrollPane();
        requeteZone = new JTextArea();

        requeteZone.setColumns(20);
        requeteZone.setEditable(false);
        requeteZone.setLineWrap(true);
        requeteZone.setRows(5);
        requeteJScroll.setViewportView(requeteZone);

        requetesJList.setModel(new AbstractListModel<String>() {
            private static final long serialVersionUID = 1L;
            String[] strings = { DEFAULT_STRING };

            public int getSize() {
                return strings.length;
            }

            public String getElementAt(int i) {
                return strings[i];
            }
        });
        requetesJList.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent evt) {
                requetesJListValueChanged(evt);
            }
        });
        requetesJScroll.setViewportView(requetesJList);

        requetesJLabel.setText(LABEL_INDEX_REQUETES);

        ajouteRequeteButton.setText(LABEL_ADD_REQUETE);
        ajouteRequeteButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                ajouteRequeteButtonActionPerformed(evt);
            }
        });
    }

    private void initializeCommentaire() {
        commentaireButton = new JButton();
        commentaireJScroll = new JScrollPane();
        commentaireZone = new JTextArea();
        commentaireButton.setBackground(COMM_BACKGROUND);
        commentaireButton.setText(LABEL_SEE_COMMS);
        commentaireButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                commentaireButtonActionPerformed(evt);
            }
        });

        commentaireZone.setColumns(COMM_COLUMN);
        commentaireZone.setLineWrap(true);
        commentaireZone.setRows(COMM_ROW);
        commentaireZone.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent evt) {
                commentaireZoneKeyPressed(evt);
            }
        });
        commentaireJScroll.setViewportView(commentaireZone);
    }

    private void layoutSetHorizontal(GroupLayout layout) {
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup().addContainerGap()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                .addComponent(ajouteRequeteButton).addComponent(requetesJLabel)
                                .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                                .addComponent(requetesJScroll, GroupLayout.PREFERRED_SIZE, HORIZONTAL_REQUETES_SIZE_CONNEXION,
                                                        GroupLayout.PREFERRED_SIZE)
                                                .addComponent(quitterButton))
                                        .addGap(HORIZONTAL_REQUETES_GAP_CONNEXION, HORIZONTAL_REQUETES_GAP_CONNEXION, HORIZONTAL_REQUETES_GAP_CONNEXION)
                                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                                .addComponent(commentaireButton).addComponent(fichierButton)
                                                .addComponent(modificationButton).addComponent(modifierJlabel)
                                                .addComponent(commentaireJScroll, GroupLayout.PREFERRED_SIZE, HORIZONTAL_COMMS_SIZE_CONNEXION,
                                                        GroupLayout.PREFERRED_SIZE))
                                        .addGap(HORIZONTAL_COMMS_GAP_CONNEXION, HORIZONTAL_COMMS_GAP_CONNEXION, HORIZONTAL_COMMS_GAP_CONNEXION)
                                        .addComponent(requeteJScroll, GroupLayout.DEFAULT_SIZE, HORIZONTAL_REQUETE_SIZE_CONNEXION, Short.MAX_VALUE)))
                        .addContainerGap()));
    }

    private void layoutSetVertical(GroupLayout layout) {
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(
                GroupLayout.Alignment.TRAILING,
                layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                        .addGroup(GroupLayout.Alignment.LEADING,
                                layout.createSequentialGroup().addGap(VERTICAL_REQUETE_GAP_CONNEXION,VERTICAL_REQUETE_GAP_CONNEXION,VERTICAL_REQUETE_GAP_CONNEXION).addComponent(requeteJScroll,
                                        GroupLayout.DEFAULT_SIZE, VERTICAL_REQUETE_SIZE_CONNEXION, Short.MAX_VALUE))
                        .addGroup(layout.createSequentialGroup().addGap(VERTICAL_AJOUT_REQUETE_GAP_CONNEXION, VERTICAL_AJOUT_REQUETE_GAP_CONNEXION, VERTICAL_AJOUT_REQUETE_GAP_CONNEXION).addComponent(ajouteRequeteButton)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(requetesJLabel)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(requetesJScroll, GroupLayout.DEFAULT_SIZE, VERTICAL_REQUETES_SIZE_CONNEXION,
                                                        Short.MAX_VALUE)
                                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(quitterButton))
                                        .addGroup(layout.createSequentialGroup().addComponent(commentaireButton)
                                                .addGap(VERTICAL_MODIFIER_GAP_CONNEXION, VERTICAL_MODIFIER_GAP_CONNEXION, VERTICAL_MODIFIER_GAP_CONNEXION).addComponent(modifierJlabel)
                                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(fichierButton)
                                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(modificationButton)
                                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(commentaireJScroll, GroupLayout.DEFAULT_SIZE, VERTICAL_COMM_SIZE_CONNEXION,
                                                        Short.MAX_VALUE)))))
                        .addContainerGap()));
    }

    private void ajouteRequeteButtonActionPerformed(ActionEvent evt) {
        this.setVisible(false);
        FenetreNouvelleRequete nouvelleRequete = new FenetreNouvelleRequete(utilisateur, this);
    }

    private void requetesJListValueChanged(ListSelectionEvent evt) {
        updateRequete();
    }

    private void quitterButtonActionPerformed(ActionEvent evt) {
        try {
            BanqueRequetes.getInstance().exporterRequetes();
            System.exit(0);
        } catch (IOException ex) {
            Logger.getLogger(FenetreClientConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void fichierButtonActionPerformed(ActionEvent evt) {
        fileChooser.showOpenDialog(frame);
        file = fileChooser.getSelectedFile();
        try {
            requetes.get(requetesJList.getSelectedIndex()).setFichier(file);
        } catch (IndexOutOfBoundsException e) {
        }

        File f = fileChooser.getSelectedFile();
        if (f != null) {
            if (file.exists()) {
                FenetreAlerte ok = new FenetreAlerte();
                ok.setVisible(true);
            }
        }
    }

    private void modificationButtonActionPerformed(ActionEvent evt) {
        commentaireZone.setVisible(true);
        commentaireZone.requestFocus();
    }

    private void commentaireButtonActionPerformed(ActionEvent evt) {
        if (requeteToShow != null) {
            if (!showComments) {
                String s = "";
                for (Commentaire c : requeteToShow.getCommentaires()) {
                    s += c.toString();
                }
                requeteZone.setText(s);
                showComments = true;
                commentaireButton.setText(LABEL_SEE_REQUETE);
                fichierButton.setEnabled(false);
                modificationButton.setEnabled(true);
            } else {
                updateRequete();
            }
        }
    }

    private void updateRequete() {
        requeteToShow = requetes.get(requetesJList.getSelectedIndex());
        requeteZone.setText(LABEL_SUJET + requeteToShow.getSujet() + LABEL_DESCRIPTION + requeteToShow.getDescription()
                + LABEL_CATEGORIE + requeteToShow.getCategorie().toString() + LABEL_STATUT
                + requeteToShow.getStatut().toString());
        showComments = false;
        commentaireButton.setText(LABEL_SEE_COMMS);
        fichierButton.setEnabled(true);
        modificationButton.setEnabled(false);
    }

    private void commentaireZoneKeyPressed(KeyEvent evt) {
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            requeteToShow.addCommentaire(commentaireZone.getText(), utilisateur);
            String s = "";
            for (Commentaire c : requeteToShow.getCommentaires()) {
                s += c.toString();
            }
            requeteZone.setText(s);
            commentaireZone.setVisible(false);
            modificationButton.requestFocus();
        }
    }
}
