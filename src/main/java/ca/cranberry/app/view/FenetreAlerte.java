package ca.cranberry.app.view;

import static ca.cranberry.app.utils.ISize.HORIZONTAL_GAP_ALERT;
import static ca.cranberry.app.utils.ISize.VERTICAL_CONTAINER_GAP_ALERT;
import static ca.cranberry.app.utils.ISize.VERTICAL_GAP_ALERT;
import static ca.cranberry.app.utils.IView.LABEL_OK;
import static ca.cranberry.app.utils.IView.MSG_ADD_SUCCESS;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.LayoutStyle;
import javax.swing.WindowConstants;

public class FenetreAlerte extends JFrame {

    private static final long serialVersionUID = 6317402784543062468L;
    private JButton acceptenceButton;
    private JLabel messageJLabel;

    public FenetreAlerte() {
        initializeMessage();
        initializeAcceptence();
        initComponents();
    }

    private void initComponents() {

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layoutSetHotizontal(layout);
        layoutSetVertical(layout);

        pack();
    }

    private void layoutSetVertical(GroupLayout layout) {
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup().addGap(VERTICAL_GAP_ALERT, VERTICAL_GAP_ALERT, VERTICAL_GAP_ALERT).addComponent(messageJLabel)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(acceptenceButton)
                        .addContainerGap(VERTICAL_CONTAINER_GAP_ALERT, Short.MAX_VALUE)));
    }

    private void layoutSetHotizontal(GroupLayout layout) {
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING,
                        layout.createSequentialGroup().addContainerGap(71, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                                        .addComponent(acceptenceButton).addComponent(messageJLabel))
                                .addGap(HORIZONTAL_GAP_ALERT, HORIZONTAL_GAP_ALERT, HORIZONTAL_GAP_ALERT)));
    }

    private void initializeAcceptence() {
        acceptenceButton = new JButton();
        acceptenceButton.setText(LABEL_OK);
        acceptenceButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
    }

    private void initializeMessage() {
        messageJLabel = new JLabel();
        messageJLabel.setText(MSG_ADD_SUCCESS);
    }

    private void jButton1ActionPerformed(ActionEvent evt) {
        this.setVisible(false);
    }

}
