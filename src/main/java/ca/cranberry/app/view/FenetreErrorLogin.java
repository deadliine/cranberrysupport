package ca.cranberry.app.view;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;
import static ca.cranberry.app.utils.IView.*;
import static ca.cranberry.app.utils.ISize.*;

public class FenetreErrorLogin extends JFrame {

    private static final long serialVersionUID = -3590101893049611104L;
    private JLabel messageJLabel;
    private JButton retourneButton;

    public FenetreErrorLogin() {
        initializeMessage();
        initializeRetourneButton();
        initComponents();
        this.setVisible(true);
    }

    private void initComponents() {

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layoutSetHorizontal(layout);
        layoutSetVertical(layout);

        pack();
    }

    private void layoutSetVertical(GroupLayout layout) {
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup().addGap(VERTICAL_MSG_GAP_ERR_LOGIN, VERTICAL_MSG_GAP_ERR_LOGIN, VERTICAL_MSG_GAP_ERR_LOGIN).addComponent(messageJLabel)
                        .addGap(VERTICAL_RETURN_BUTTON_GAP_LOGIN,VERTICAL_RETURN_BUTTON_GAP_LOGIN,VERTICAL_RETURN_BUTTON_GAP_LOGIN).addComponent(retourneButton).addContainerGap(VERTICAL_CONTAINER_GAP_LOGIN, Short.MAX_VALUE)));
    }

    private void layoutSetHorizontal(GroupLayout layout) {
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(GroupLayout.Alignment.TRAILING,
                        layout.createSequentialGroup().addContainerGap(HORIZONTAL_MSG_GAP_LOGIN, Short.MAX_VALUE).addComponent(messageJLabel)
                                .addGap(HORIZONTAL_MSG_CONTAINER_GAP_LOGIN, HORIZONTAL_MSG_CONTAINER_GAP_LOGIN, HORIZONTAL_MSG_CONTAINER_GAP_LOGIN))
                .addGroup(layout.createSequentialGroup().addGap(HORIZONTAL_RETURN_BUTTON_GAP_LOGIN, HORIZONTAL_RETURN_BUTTON_GAP_LOGIN, HORIZONTAL_RETURN_BUTTON_GAP_LOGIN).addComponent(retourneButton)
                        .addContainerGap(HORIZONTAL_RETURN_BUTTON_CONTAINER_GAP_LOGIN, Short.MAX_VALUE)));
    }

    private void initializeRetourneButton() {
        retourneButton = new JButton();
        retourneButton.setText("Recommencer");
        retourneButton.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                revenirLblMouseClicked(evt);
            }
        });
    }

    private void initializeMessage() {
        messageJLabel = new JLabel();
        messageJLabel.setText(MSG_BAD_LOGIN);
    }

    private void revenirLblMouseClicked(MouseEvent evt) {
        this.setVisible(false);
        FrenetreClient nouvelEssai = new FrenetreClient();
    }

}
