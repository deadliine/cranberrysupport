package ca.cranberry.app.view;

import static ca.cranberry.app.utils.ISize.HORIZONTAL_CONTAINER_GAP_RAPPORT;
import static ca.cranberry.app.utils.ISize.HORIZONTAL_RAPPORT_GAP_RAPPORT;
import static ca.cranberry.app.utils.ISize.HORIZONTAL_RAPPORT_SIZE_RAPPORT;
import static ca.cranberry.app.utils.ISize.HORIZONTAL_RETURN_BUTTON_GAP_RAPPORT;
import static ca.cranberry.app.utils.ISize.RAPPORT_COLUMN;
import static ca.cranberry.app.utils.ISize.RAPPORT_ROW;
import static ca.cranberry.app.utils.ISize.VERTICAL_CONTAINER_GAP_RAPPORT;
import static ca.cranberry.app.utils.ISize.VERTICAL_RAPPORT_SIZE_RAPPORT;
import static ca.cranberry.app.utils.IView.LABEL_REVENIR;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle;
import javax.swing.WindowConstants;

public class FenetreRapport extends JFrame {

    private static final long serialVersionUID = 1479450117501924912L;
    private JButton retournButton;
    private JScrollPane rapportScroll;
    private JTextArea rapportZone;

    public FenetreRapport(String t) {
        initializeRapport();
        initializeRetourn();
        initComponents();
        rapportZone.setText(t);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    }

    private void initComponents() {

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layoutSetHorizontal(layout);
        layoutSetVertical(layout);

        pack();
    }

    private void initializeRetourn() {
        retournButton = new JButton();
        retournButton.setText(LABEL_REVENIR);
        retournButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
    }

    private void initializeRapport() {
        rapportScroll = new JScrollPane();
        rapportZone = new JTextArea();
        rapportZone.setColumns(RAPPORT_COLUMN);
        rapportZone.setEditable(false);
        rapportZone.setLineWrap(true);
        rapportZone.setRows(RAPPORT_ROW);
        rapportScroll.setViewportView(rapportZone);
    }

    private void layoutSetHorizontal(GroupLayout layout) {
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout
                .createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup().addGap(HORIZONTAL_RAPPORT_GAP_RAPPORT, HORIZONTAL_RAPPORT_GAP_RAPPORT, HORIZONTAL_RAPPORT_GAP_RAPPORT).addComponent(rapportScroll,
                                GroupLayout.PREFERRED_SIZE, HORIZONTAL_RAPPORT_SIZE_RAPPORT, GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup().addGap(HORIZONTAL_RETURN_BUTTON_GAP_RAPPORT,HORIZONTAL_RETURN_BUTTON_GAP_RAPPORT,HORIZONTAL_RETURN_BUTTON_GAP_RAPPORT).addComponent(retournButton)))
                .addContainerGap(HORIZONTAL_CONTAINER_GAP_RAPPORT, Short.MAX_VALUE)));
    }

    private void layoutSetVertical(GroupLayout layout) {
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup().addContainerGap()
                        .addComponent(rapportScroll, GroupLayout.PREFERRED_SIZE, VERTICAL_RAPPORT_SIZE_RAPPORT, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(retournButton)
                        .addContainerGap(VERTICAL_CONTAINER_GAP_RAPPORT, Short.MAX_VALUE)));
    }

    private void jButton1ActionPerformed(ActionEvent evt) {
        this.setVisible(false);
    }
}
