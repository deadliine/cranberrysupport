package ca.cranberry.app.view;

import static ca.cranberry.app.utils.ISize.HORIZONTAL_CONFIRME_GAP_TECH_LOGIN;
import static ca.cranberry.app.utils.ISize.HORIZONTAL_CONFIRME_SIZE_TECH_LOGIN;
import static ca.cranberry.app.utils.ISize.HORIZONTAL_CONTAINER_GAP_TECH_LOGIN;
import static ca.cranberry.app.utils.ISize.HORIZONTAL_FIELD_SIZE_TECH_LOGIN;
import static ca.cranberry.app.utils.ISize.HORIZONTAL_TOP_GAP_TECH_LOGIN;
import static ca.cranberry.app.utils.ISize.VERTICAL_CONFIMATION_GAP_TECH_LOGIN;
import static ca.cranberry.app.utils.ISize.VERTICAL_FIELD_GAP_TECH_LOGIN;
import static ca.cranberry.app.utils.ISize.VERTICAL_TOP_GAP_TECH_LOGIN;
import static ca.cranberry.app.utils.IView.LABEL_ANNULER;
import static ca.cranberry.app.utils.IView.LABEL_NAME;
import static ca.cranberry.app.utils.IView.LABEL_OK;
import static ca.cranberry.app.utils.IView.LABEL_PASSWD;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.WindowConstants;

import ca.cranberry.app.beans.Utilisateur;
import ca.cranberry.app.exception.NomUtilisateurException;
import ca.cranberry.app.service.BanqueUtilisateurs;

public class FenetreTechnicienConnection extends JFrame {

    private static final long serialVersionUID = 3708643579226129370L;
    private JButton annulerButton;
    private JTextField motPasseField;
    private JLabel motPasseJLabel;
    private JLabel nomUtilsateurJLabel;
    private JButton confirmationButton;
    private JTextField utilisateurField;

    public FenetreTechnicienConnection() {
        initializeUtilisateur();
        initializeMotPasse();
        initializeConfirmation();
        initializeAnnuler();
        initComponents();
        this.setVisible(true);
    }

    private String nomUtil;
    private String motdepasse;
    private Utilisateur potentiel;

    private void initComponents() {

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layoutSetHorizontal(layout);
        layoutSetVertical(layout);

        pack();
    }

    private void initializeUtilisateur() {
        nomUtilsateurJLabel = new JLabel();
        nomUtilsateurJLabel.setText(LABEL_NAME);

        utilisateurField = new JTextField();
    }

    private void initializeMotPasse() {
        motPasseJLabel = new JLabel();
        motPasseField = new JTextField();
        motPasseJLabel.setText(LABEL_PASSWD);
    }

    private void initializeConfirmation() {
        confirmationButton = new JButton();
        confirmationButton.setText(LABEL_OK);
        confirmationButton.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                okBtnMouseClicked(evt);
            }
        });
    }

    private void initializeAnnuler() {
        annulerButton = new JButton();
        annulerButton.setText(LABEL_ANNULER);
        annulerButton.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                annulerBtnMouseClicked(evt);
            }
        });
    }

    private void layoutSetHorizontal(GroupLayout layout) {
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout
                .createSequentialGroup().addGap(HORIZONTAL_TOP_GAP_TECH_LOGIN,HORIZONTAL_TOP_GAP_TECH_LOGIN,HORIZONTAL_TOP_GAP_TECH_LOGIN)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(nomUtilsateurJLabel)
                        .addComponent(motPasseJLabel))
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(confirmationButton, GroupLayout.PREFERRED_SIZE, HORIZONTAL_CONFIRME_SIZE_TECH_LOGIN,
                                        GroupLayout.PREFERRED_SIZE)
                                .addGap(HORIZONTAL_CONFIRME_GAP_TECH_LOGIN,HORIZONTAL_CONFIRME_GAP_TECH_LOGIN,HORIZONTAL_CONFIRME_GAP_TECH_LOGIN).addComponent(annulerButton))
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                .addComponent(motPasseField)
                                .addComponent(utilisateurField, GroupLayout.DEFAULT_SIZE, HORIZONTAL_FIELD_SIZE_TECH_LOGIN, Short.MAX_VALUE)))
                .addContainerGap(HORIZONTAL_CONTAINER_GAP_TECH_LOGIN, Short.MAX_VALUE)));
    }

    private void layoutSetVertical(GroupLayout layout) {
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout
                .createSequentialGroup().addGap(VERTICAL_TOP_GAP_TECH_LOGIN,VERTICAL_TOP_GAP_TECH_LOGIN,VERTICAL_TOP_GAP_TECH_LOGIN)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(nomUtilsateurJLabel)
                        .addComponent(utilisateurField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                GroupLayout.PREFERRED_SIZE))
                .addGap(VERTICAL_FIELD_GAP_TECH_LOGIN,VERTICAL_FIELD_GAP_TECH_LOGIN,VERTICAL_FIELD_GAP_TECH_LOGIN)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(motPasseJLabel)
                        .addComponent(motPasseField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                GroupLayout.PREFERRED_SIZE))
                .addGap(VERTICAL_FIELD_GAP_TECH_LOGIN,VERTICAL_FIELD_GAP_TECH_LOGIN,VERTICAL_FIELD_GAP_TECH_LOGIN).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(confirmationButton).addComponent(annulerButton))
                .addContainerGap(VERTICAL_CONFIMATION_GAP_TECH_LOGIN, Short.MAX_VALUE)));
    }

    private void okBtnMouseClicked(MouseEvent evt) {
        try {
            nomUtil = utilisateurField.getText();
            motdepasse = motPasseField.getText();

            try {
            	BanqueUtilisateurs bu= BanqueUtilisateurs.getInstance();
            	bu.importerUtilisateur();
                potentiel = bu.getUtilisateurByNomUtilisateur(nomUtil);
                if (potentiel.login(nomUtil, motdepasse)) {
                    this.setVisible(false);
                    FenetreTechnicien fenetreTech = new FenetreTechnicien(potentiel);
                } else {
                    FenetreErrorLogin mauvaisLogin = new FenetreErrorLogin();
                    this.setVisible(false);
                }
            } catch (NomUtilisateurException e) {
                FenetreErrorLogin mauvaisLogin = new FenetreErrorLogin();
                this.setVisible(false);
            }
        } catch (IOException ex) {
            Logger.getLogger(FenetreTechnicienConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void annulerBtnMouseClicked(MouseEvent evt) {
        this.setVisible(false);
        FenetreCranberry retour = new FenetreCranberry();
    }

}
