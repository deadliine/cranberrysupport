package ca.cranberry.app.view;

import static ca.cranberry.app.utils.IColor.DEFAULT_BACKGROUND;
import static ca.cranberry.app.utils.ISize.HORIZONTAL_CONTAINER_GAP_DEFAULT_WINDOW;
import static ca.cranberry.app.utils.ISize.HORIZONTAL_MSG_START_GAP_DEFAULT_WINDOW;
import static ca.cranberry.app.utils.ISize.HORIZONTAL_TECH_BUTTON_GAP_DEFAULT_WINDOW;
import static ca.cranberry.app.utils.ISize.HORIZONTAL_UTIL_BUTTON_GAP_DEFAULT_WINDOW;
import static ca.cranberry.app.utils.IView.LABEL_DEMARRAGE;
import static ca.cranberry.app.utils.IView.LABEL_START_AS;
import static ca.cranberry.app.utils.IView.LABEL_TECH;
import static ca.cranberry.app.utils.IView.LABEL_UTIL;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;

public class FenetreCranberry extends JFrame {

    private static final long serialVersionUID = 7280827901721994446L;
    private JButton utilisateurButton;
    private JLabel messageDemarrageJlabel;
    private JButton technicienButton;

    public FenetreCranberry() {

        initializeMessageDemarrage();
        initializeTechnicienButton();
        initializeUtilisateurButton();
        initComponents();

        this.setVisible(true);
    }

    private void initComponents() {

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBackground(DEFAULT_BACKGROUND);
        setName(LABEL_DEMARRAGE);

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layoutSetHorizontal(layout);
        layoutSetVertical(layout);

        pack();
    }

    private void initializeMessageDemarrage() {
        messageDemarrageJlabel = new JLabel();
        messageDemarrageJlabel.setText(LABEL_START_AS);
    }

    private void initializeTechnicienButton() {
        technicienButton = new JButton();
        technicienButton.setText(LABEL_TECH);
        technicienButton.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                techBtnMouseClicked(evt);
            }
        });
    }

    private void initializeUtilisateurButton() {
        utilisateurButton = new JButton();
        utilisateurButton.setText(LABEL_UTIL);
        utilisateurButton.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                clientBtnMouseClicked(evt);
            }
        });
    }

    private void layoutSetHorizontal(GroupLayout layout) {
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup().addGap(HORIZONTAL_MSG_START_GAP_DEFAULT_WINDOW, HORIZONTAL_MSG_START_GAP_DEFAULT_WINDOW, HORIZONTAL_MSG_START_GAP_DEFAULT_WINDOW).addComponent(messageDemarrageJlabel)
                        .addGap(HORIZONTAL_UTIL_BUTTON_GAP_DEFAULT_WINDOW, HORIZONTAL_UTIL_BUTTON_GAP_DEFAULT_WINDOW, HORIZONTAL_UTIL_BUTTON_GAP_DEFAULT_WINDOW).addComponent(utilisateurButton).addGap(HORIZONTAL_TECH_BUTTON_GAP_DEFAULT_WINDOW, HORIZONTAL_TECH_BUTTON_GAP_DEFAULT_WINDOW, HORIZONTAL_TECH_BUTTON_GAP_DEFAULT_WINDOW)
                        .addComponent(technicienButton).addContainerGap(HORIZONTAL_CONTAINER_GAP_DEFAULT_WINDOW, Short.MAX_VALUE)));
    }

    private void layoutSetVertical(GroupLayout layout) {
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup().addContainerGap()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(messageDemarrageJlabel).addComponent(utilisateurButton)
                                .addComponent(technicienButton))
                        .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
    }

    private void techBtnMouseClicked(MouseEvent evt) {
        this.setVisible(false);
        FenetreTechnicienConnection technic = new FenetreTechnicienConnection();
    }

    private void clientBtnMouseClicked(MouseEvent evt) {
        this.setVisible(false);
        FrenetreClient visuelClient = new FrenetreClient();
    }

}
