package ca.cranberry.app.service;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import ca.cranberry.app.beans.Utilisateur;
import ca.cranberry.app.database.DatabaseConnection;
import ca.cranberry.app.exception.NomUtilisateurException;

public class BanqueUtilisateurs {

    private static BanqueUtilisateurs ListeUsers = null;
    private List<Utilisateur> utilisateurs;

    private BanqueUtilisateurs() throws FileNotFoundException {
        utilisateurs = new ArrayList<Utilisateur>();

    }

    public void importerUtilisateur() throws FileNotFoundException {
        Session session = DatabaseConnection.getSession();
        session.beginTransaction();
        utilisateurs = session.createQuery("from Utilisateur").getResultList();
        session.getTransaction().commit();
        session.close();
        for (Utilisateur util : utilisateurs) {
            System.out.println(util.toString());
        }
    }

    public static BanqueUtilisateurs getInstance() throws FileNotFoundException {
        if (ListeUsers == null) {
            ListeUsers = new BanqueUtilisateurs();
            ListeUsers.importerUtilisateur();
        }
        return ListeUsers;

    }

    public ArrayList<Utilisateur> getUtilisateurs(String role) {
        ArrayList<Utilisateur> users = new ArrayList<Utilisateur>();
        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).getRole().equals(role)) {
                users.add(users.get(i));
            }
        }
        return users;
    }

    public Utilisateur getUtilisateurByNomUtilisateur(String nomVoulu) throws NomUtilisateurException {
        for (int i = 0; i < utilisateurs.size(); i++) {
            if (utilisateurs.get(i).getNomUtilisateur().equalsIgnoreCase(nomVoulu)) {
                return utilisateurs.get(i);
            }

        }
        throw new NomUtilisateurException();
    }
}