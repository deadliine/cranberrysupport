package ca.cranberry.app.exception;

public class NomUtilisateurException extends Exception {
    private static final long serialVersionUID = 3090676171991920851L;

    public NomUtilisateurException() {
        super("Nom d'utilisateur not find");
    }
}
