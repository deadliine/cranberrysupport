package ca.cranberry.app.beans;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Utilisateur {

    public enum Role{

        client("CLIENT"), technicien("TECHNICIEN");
        String valeur;

        Role(String s) {
            this.valeur = s;
        }

        public String getValueString() {
            return valeur;
        }

        public static Role fromString(String text) {
            if (text != null) {
                if (Role.client.getValueString().equals(text)) {
                    return Role.client;
                } else if (Role.technicien.getValueString().equals(text)) {
                    return Role.technicien;
                }
            }
            return null;
        }
    }
    
    @Id
    @GeneratedValue(strategy=GenerationType.TABLE)
    private int id;
    private String nom;
    private String prenom;
    private String nomUtilisateur;
    private String motPasse;
    private int telephone;
    private String mail;
    private String bureau;
    @Enumerated(EnumType.STRING)
    private Role role;
    @OneToMany(fetch = FetchType.EAGER, targetEntity=Requete.class, cascade = CascadeType.ALL, mappedBy = "utilisateur")
    private List<Requete> requetes;

    
    public Utilisateur() {
        super();
        requetes = new ArrayList<Requete>();
    }

    public Utilisateur(String prenom, String nom, String nomUtilisateur, String motPasse, Role role) {
        this.prenom = prenom;
        this.nom = nom;
        this.nomUtilisateur = nomUtilisateur;
        this.motPasse = motPasse;
        this.role = role;
        this.telephone = 0;
        requetes = new ArrayList<Requete>();
    }

    public boolean login(String nomUtilisateur, String motPasse) {
        return this.nomUtilisateur.equalsIgnoreCase(nomUtilisateur) && this.motPasse.equals(motPasse);

    }

    public void ajouterRequete(Requete requete) {
        requetes.add(requete);
    }

    public ArrayList<Requete> getListStatut(Requete.Statut statut) {
        ArrayList<Requete> r = new ArrayList<Requete>();
        for (int i = 0; i < requetes.size(); i++) {
            if (requetes.get(i).getStatut().equals(statut)) {
                r.add(requetes.get(i));
            }

        }
        return r;
    }

    public String getRequeteParStatut() {
        String ouvert = "Statut ouvert: ";
        int ouv = 0;
        String enTraitement = "Statut En traitement: ";
        int tr = 0;
        String succes = "Statut succès: ";
        int su = 0;
        String abandon = "Statut abandonnée: ";
        int ab = 0;
        String parStatut = "";
        for (int i = 0; i < requetes.size(); i++) {
            switch (requetes.get(i).getStatut()) {
            case ouvert:
                ouv++;
                break;
            case enTraitement:
                tr++;
                break;
            case finalSucces:
                su++;
                break;
            case finalAbandon:
                ab++;
                break;
            default:
                break;
            }
        }

        parStatut = ouvert + ouv + "\n" + enTraitement + tr + "\n" + succes + su + "\n" + abandon + ab + "\n";
        return parStatut;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNomUtilisateur() {
        return nomUtilisateur;
    }

    public void setNomUtilisateur(String nomUtilisateur) {
        this.nomUtilisateur = nomUtilisateur;
    }

    public String getMotPasse() {
        return motPasse;
    }

    public void setMotPasse(String motPasse) {
        this.motPasse = motPasse;
    }

    public int getTelephone() {
        return telephone;
    }

    public void setTelephone(int telephone) {
        this.telephone = telephone;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getBureau() {
        return bureau;
    }

    public void setBureau(String bureau) {
        this.bureau = bureau;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public List<Requete> getRequetes() {
        return requetes;
    }

    public void setRequetes(List<Requete> requetes) {
        this.requetes = requetes;
    }

    @Override
    public String toString() {
        return "Utilisateur [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", nomUtilisateur=" + nomUtilisateur
                + ", motPasse=" + motPasse + ", telephone=" + telephone + ", mail=" + mail + ", bureau=" + bureau
                + ", role=" + role + "]";
    }
    
    
}