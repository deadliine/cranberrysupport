package ca.cranberry.app.database;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class DatabaseConnection {

    private static SessionFactory factory;

    private static void generateFactory(){
        factory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
    }

    public static Session getSession() {         
        Session sess = null;  
        generateFactory();
        try {         
            sess = factory.getCurrentSession();  
        } catch (org.hibernate.HibernateException he) {  
            sess = factory.openSession();     
        }             
        return sess;
     } 
}
