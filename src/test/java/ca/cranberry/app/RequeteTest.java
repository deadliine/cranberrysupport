package ca.cranberry.app;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ca.cranberry.app.beans.Requete;
import ca.cranberry.app.beans.Utilisateur;

class RequeteTest {
	private Utilisateur client;
	private Utilisateur technicien;
	private Requete requete;
	private String prenom = "prenom";
	private String nom = "nom";
	private String password = "password";
	private String roleClient = "client";
	private String roleTechnicien = "technicien";
	private String username = "username";
	private String sujet = "sujet";
	private String description = "description";
	private Requete.Categorie cat = Requete.Categorie.autre;

	@BeforeEach
	void setUp() throws Exception {
		client = new Utilisateur(prenom, nom, username, password, Utilisateur.Role.fromString(roleClient));
		technicien = new Utilisateur(prenom, nom, username, password, Utilisateur.Role.fromString(roleTechnicien));
		requete = new Requete(sujet, description, client, cat);
	}

	@AfterEach
	void tearDown() throws Exception {

	}

	@Test
	void finaliserTest() {
		requete.setTech(technicien);
		requete.finaliser(Requete.Statut.finalSucces);
		assertTrue(technicien.getListStatut(Requete.Statut.finalSucces).size() > 0);
	}

	@Test
	void addCommentaireTest() {
		requete.addCommentaire(description, technicien);
		assertTrue(requete.getCommentaires().size() > 0);
	}
}
